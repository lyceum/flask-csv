# flask-csv

Une toute petite application serveur en Python pour montrer comment traiter les
données d'un formulaire `html` côté serveur.

Cette application est destinée à des personnalités de **première NSI**, les données
sont donc stockées sous forme de fichier `csv`, puisque les bases de données ne
sont abordées qu'en terminale.

## Lancer l'application

Pour lancer vous devez avoir une distribution python 3 avec le paquet `flask`
installé(Par défaut sur [anaconda](https://www.anaconda.com/products/individual)
ou [winpython](https://winpython.github.io/)).

L'application peut être lancée avec python: `python flask-csv.py`.

Ouvrir ensuite la page web dans le navigateur à l'adresse locale:
<http://127.0.0.1:5000/>

## Problèmes de lancement

Il se peut qu'en installant python, celui-ci n'ait pas ajouté à la variable
système `PATH`.

Il faut alors indiquer le chemin complet vers l'exécutable python pour lancer
les scripts `python`.

Pour le trouver, utiliser le chemin complet vers le dossier où vous avez installé python, par exemple si votre utilisateur s'appelle `toto`:

- windows: `C:\Users\toto\Anaconda3\python.exe`
- macOS: `~/anaconda/bin/python` or `/Users/toto/anaconda/bin/python`
- Linux: `~/anaconda/bin/python` or `/home/toto/anaconda/bin/python`

## Crédits

- [flask](http://flask.pocoo.org/) pour faire tourner l'application serveur.
- [bootstrap](https://getbootstrap.com/) pour la mise en forme css.
