import csv
from flask import Flask, render_template, redirect, flash, url_for, request, g

app = Flask(__name__, template_folder="pages")
app.secret_key = b"X9#92eNw#UjY~nP%"

# les données sont stockées dans un fichier csv plutôt qu'une bdd en première
CSV_FILE = "data.csv"


@app.route("/")
def index_page():
    "Page d'accueil qui liste les acteurs."

    return render_template("index.html", data=g.data)


@app.route("/form_post", methods=["GET", "POST"])
def post_form_page():
    """Page d'ajout d'un acteur avec un formulaire POST.

    Dans le cas d'un requête POST, les données sont récupérées dans la variable
    request.form
    """

    if request.method == "POST":
        # on récupère les données du formulaire
        post_data = request.form
        # conversion en dictionnaire des données
        acteur = post_data.to_dict()
        print(acteur)
        g.data.append(acteur)
        print(g.data)
        # ecriture des données
        ecrit_csv(g.data)
        # Ajoute un message
        flash("Ajout des données: " + str(acteur))
        # retourne sur la page d'accueil
        return redirect(url_for("index_page"))

    return render_template("form_post.html")


@app.route("/form_get")
def get_form_page():
    """Page d'ajout d'un acteur avec un formulaire GET.

    Dans le cas d'un requête GET, les données sont récupérées dans la variable
    request.args
    """
    filtered_data = []

    if request.args:
        # on récupère les données du formulaire
        get_data = request.args
        form_data = get_data.to_dict()
        # Affiche les données du formulaire
        flash("Données du formulaire" + str(form_data))

        # TODO: Filtrer les données en fonction des données du formulaire
        filtered_data = g.data

    return render_template("form_get.html", data=filtered_data)


# Gestion des données du fichier csv
@app.before_request
def before_request_func():
    """Fonction de chargement des données csv

    Les données sont stockées sous forme d'une liste de dictionnaire
    et stockées dans la variable globale g de flask
    """
    print("Chargement des données csv")
    with open(CSV_FILE) as csvfile:
        reader = csv.DictReader(csvfile)
        g.data = [ligne for ligne in reader]


def ecrit_csv(data):
    """Ecrit le fichier csv avec les données à jour"""
    with open(CSV_FILE, "w", encoding="utf-8") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=["nom", "prenom", "date_naissance"])
        writer.writeheader()
        for acteur in data:
            writer.writerow(acteur)


if __name__ == "__main__":
    app.run(debug=True)
